import { HttpStatus, baseUrl } from "../config/constant.js";
import successResponse from "../helper/successResponse.js";
import { User } from "../schema/model.js";
import expressAsyncHandler from "express-async-handler";
import { sendMail } from "../utils/sendMail.js";
import { comparePassword, hashPassword } from "../utils/hashing.js";
import { generateToken, verifyToken } from "../utils/token.js";
import { Token } from "../schema/model.js";

export let createUser = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;                                      //taking data from postman
  data.isVerify = false                                     //we set isVerify and isDeactivate to false in code itself and not let the user decide
  data.isDeactivate = false                                 
  let email = data.email                                    //getting email and storing in variable
  let user = await User.findOne({ email:email });           //Checking if the email is in DB
  
  if (user) {                                               //If it is then show duplicate email error
    let error = new Error("Duplicate email.");              
    error.statusCode = 409;
    throw error;
  }else{                                                    //else hash the password and create User
    let _hashPassword = await hashPassword(data.password);
  data.password = _hashPassword;
  let result = await User.create(req.body);
  delete result._doc.password;                              //delete password to not show it in response
  let infoObj = {                                           //setting infoObj and expireInfo for generating token
    id: result._id,
    role: result.role,
  };
  let expireInfo = {
    expiresIn: "1d",
  };
  let token = await generateToken(infoObj, expireInfo);    //Calling the generate token function
  await Token.create({ token });
  let link = `${baseUrl}/verify-email?token=${token}`      //Giving link and sending it to email for email verification
  await sendMail({
    from: '"Nitan Thapa" <uniquekc425@gmail.com>',         //This is the text that is shown in (sent by)
    to: [data.email],
    subject: "Email verification",
    html: `<h1>
    Verify Email
    <a href = "${link}">Click to verify</a>               
    <h1>`,
  });

  successResponse(res, HttpStatus.CREATED, "User created successfully", result);
  }
  
});

//localhost:8000/users/verify-email?token=1234234
export let verifyEmail = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;    //getting id from query and setting it in a variable
  console.log(id)
  let tokenId = req.token.tokenId   //sent token inside isAuthenticated and received tokenId through it
  console.log(tokenId)
  let result = await User.findByIdAndUpdate(         //This line updates the user document in the database with the provided id. 
    id,
    { isVerify: true },    //isVerify is set to true, initially its false
    { new: true }          //this updates the response at once and need not hit the postman twice
  );
  delete result._doc.password;    //password should not be shown so we delete it
  await Token.findByIdAndDelete(tokenId)    //No use

  successResponse(
    res,
    HttpStatus.CREATED,
    "Email verified successfully.",
    result
  );
});

export let loginUser = expressAsyncHandler(async (req, res, next) => {
  let email = req.body.email;         //getting email from postman and setting it in a variable
  let password = req.body.password;   //getting password from postman and setting it in a variable
  let data = await User.findOne({ email: email }); //if not present null, if present, gives output in object
  
  if(data.isDeactivate) {
    await User.findByIdAndUpdate(data._id, {isDeactivate: false});  //isDeactivate false when logged in
   }

  if (!data) {                        //if it doesn't match the database's email throw this
    let error = new Error("Credential doesn't match");
    error.statusCode = 401;
    throw error;
  } else 
  {
    let isValidPassword = await comparePassword(password, data.password);   //checking if password matches
    if (!isValidPassword) {                            //if it doesn't match the database's password, throw error
      let error = new Error("Credential doesn't match");
      error.statusCode = 401;
      throw error;
    } else {
      if (!data.isVerify) {                  //If it is not verified, throw error

        let error = new Error("Please Verify Your Account First.");
        error.statusCode = 401;
        throw error;
      } else {                    //If it is verified, generate token
        let infoObj = {
          id: data._id,
          role: data.role,
        };
        let expireInfo = {
          expiresIn: "365d",
        };
        let token = await generateToken(infoObj, expireInfo);      //calling the generateToken function
        await Token.create({ token });             //Theres a separate DB for Token so we are saving it there
        successResponse(res, HttpStatus.CREATED, "Login Successfully", token);
      }
    }
    // console.log("isValidPassword", isValidPassword);
  }
});

//using isAuthenticated function
export let myProfile = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let result = await User.findById(id);
  successResponse(res, HttpStatus.OK, "My-profile read successfully", result);
});



//This is without isAuthenticated code

// export let myProfile = expressAsyncHandler(async (req, res, next) => {
//   //to get token
//   let bearerToken = req.headers.authorization;
//   //bearer token has different data, so we need to split it to get it in an array
//   let tokenArr = bearerToken.split(" ");
//   //Now, we are indexing the token and saving it in token variable
//   let token = tokenArr[1];
//   //This finds the Token from DB and sets it in _token
//   let _token = await Token.findOne({ token: token });

//   if (!_token) {                            //If token does not match, throw error
//     let error = new Error("Token is not valid");
//     error.statusCode = 401;
//     throw error;
//   } else {
//     let info = await verifyToken(token);       //verifying token using jwt
//     console.log(info);

//     let id = info.id;                          // user Id

//     let user = await User.findById(id);        //Find that id in DB

//     successResponse(res, HttpStatus.OK, "My-profile read successfully", user);
//   }
// });


export let forgetPassword = expressAsyncHandler(async (req, res, next) => {       
  let email=req.body.email          //have to send email in postman 

  let data= await User.findOne({email})       //checking if that email exists
  console.log(data)
  let infoObj = {                             //giving infoObj and expireInfo for token generation
    id: data._id,                             //id and role is always passed in infoObj
    role: data.role,
  };
  let expireInfo = {                          //we pass expire time in expireInfo
    expiresIn: "1d",
  };

  let token = await generateToken(infoObj,expireInfo)       //calling the generateToken function

   await Token.create({token})                              //saved to database

   await sendMail({
    from: '"Nitan Thapa" <uniquekc425@gmail.com>',          //sending token in mail
    to: [data.email],
    subject: "Email verification",
    html: `<h1>
    Verify Email
    ${token}
    <h1>`,
  });
  
  successResponse(res, HttpStatus.OK, "Mail sent successfully");
});


export let resetPassword = expressAsyncHandler(async (req, res, next) => {
  console.log(req.info); // Checking if isAuthenticated middleware is providing req.info
  console.log(req.token); // Checking if isAuthenticated middleware is providing req.token

  let id = req.info.id; // Saving the id in a variable
  let tokenId = req.token.tokenId; // Saving token id in a variable

  let newPassword = await hashPassword(req.body.password); // Hashing the new password


  //now send new password in postman

  await User.findByIdAndUpdate(id, { password: newPassword }, { new: true }); // Updating the new password in the database
  await Token.findByIdAndDelete(tokenId); // Deleting the token as it is no longer needed

  successResponse(res, HttpStatus.OK, "Password updated successfully");
});

export let logout = expressAsyncHandler(async (req, res, next) => {
  let tokenId = req.token.tokenId;
  let result = await Token.findByIdAndDelete(tokenId);
  successResponse(res, HttpStatus.OK, "logout successfully", result);
});

export let updateMyProfile = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let data = req.body;
  delete data.email;
  delete data.password;
  delete data.isVerify;
  // console.log(data);
  let result = await User.findByIdAndUpdate(id, data, { new: true });
  delete result._doc.password;
  successResponse(res, HttpStatus.OK, "update successfully", result);
});

export let updatePassword = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let tokenId = req.token.tokenId;

  let _hashPassword = await hashPassword(req.body.password);
  let data = { password: _hashPassword };
  let result = await User.findByIdAndUpdate(id, data, { new: true });
  delete result._doc.password;

  await Token.findByIdAndDelete(tokenId);
  successResponse(res, HttpStatus.OK, "updated password successfully", result);
});

export let deactivate = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id
 let user = await User.findByIdAndUpdate(id,{ isDeactivate: true},{new:true})
 successResponse(res, HttpStatus.OK, "Account deactivated successfully", user);

})
//only verified user can create account (alternative to register)

// export let createUser = expressAsyncHandler(async (req, res, next) => {
//   let data = req.body;
//   let _hashPassword = await hashPassword(data.password);
//   data.password = _hashPassword;
//   // let result = await User.create(req.body);
//   // delete result._doc.password;
//   await sendMail({
//     from: '"Nitan Thapa" <uniquekc425@gmail.com>',
//     to: [data.email],
//     subject: "Email verification",
//     html: `<h1>
//     Verify Email
//     ${data}
//     <h1>`,
//   });

//   successResponse(res, HttpStatus.CREATED, "User created successfully");
// });

// export let verifyEmail = expressAsyncHandler(async (req, res, next) => {
//   let data = req.body;

//   let result = await User.create(data);
//   delete result._doc.password;
//   successResponse(
//     res,
//     HttpStatus.CREATED,
//     "Email verified successfully.",
//     result
//   );
// });

export let readUserDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await User.findById(req.params.id);
  successResponse(res, HttpStatus.OK, "Read User details successfully", result);
});

//get all
//update details (id)
//delete (id)

export let readAllUser = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await User.find({ name: "nitan" });

    successResponse(res, HttpStatus.OK, "Read User  successfully", result);
  } catch (error) {
    errorResponse(res, HttpStatus.BAD_REQUEST, error.message);
  }
});

export let deleteUser = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await User.findByIdAndDelete(req.params.id);
    successResponse(res, HttpStatus.OK, "Delete User  successfully.", result);
  } catch (error) {
    error.statusCode = HttpStatus.BAD_REQUEST;
    next(error);
  }
});

export let updateUser = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await User.findByIdAndUpdate(req.params.id, req.body);
    successResponse(
      res,
      HttpStatus.CREATED,
      "Update User  successfully.",
      result
    );
  } catch (error) {
    error.statusCode = HttpStatus.BAD_REQUEST;
    next(error);
  }
});
